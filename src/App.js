import React from "react";
import "./App.css";
import "bootstrap/dist/css/bootstrap.min.css";

import UploadImage from "./components/image-upload.component";

function App() {
  return (
    <div className="container">
      <h4 className="text-center">Загрузите фото продуктов.</h4>

      <div className="content">
        <UploadImage />
      </div>
    </div>
  );
}

export default App;
