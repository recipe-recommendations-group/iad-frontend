import React, { Component } from "react";
import UploadService from "../services/file-upload.service";
import Link from "react"

export default class UploadImage extends Component {
  constructor(props) {
    super(props);
    this.selectFile = this.selectFile.bind(this);
    this.upload = this.upload.bind(this);

    this.state = {
      currentFile: undefined,
      previewImage: undefined,
      progress: 0,
      recipesCount: 0,

      detectedIngredients: [],
      recipes: [],
    };
  }

  // componentDidMount() {
  //   UploadService.getFiles().then((response) => {
  //     this.setState({
  //       imageInfos: response.data,
  //     });
  //   });
  // }

  selectFile(event) {
    this.setState({
      currentFile: event.target.files[0],
      previewImage: URL.createObjectURL(event.target.files[0]),
      progress: 0,
      recipesCount: 0,

      detectedIngredients: [],
      recipes: []
    });
  }

  upload() {
    this.setState({
      progress: 0,
    });

    UploadService.upload(this.state.currentFile, (event) => {
      this.setState({
        progress: Math.round((100 * event.loaded) / event.total),
      });
    })
      .then((response) => {
        this.setState({
          detectedIngredients: response.data.detectedIngredients,
          recipesCount: response.data.recipesCount,
          recipes: response.data.recipes
        });
        return response.data;
      })
      .catch((err) => {
        alert("Unknown error!")
        this.setState({
          progress: 0,
          currentFile: undefined,
        });
      });
  }

  render() {
    const {
      currentFile,
      previewImage,
      progress,
      recipesCount,
      detectedIngredients,
      recipes
    } = this.state;

    return (
      <div>
        {previewImage && (
          <div>
            <img className="preview w-75 rounded mx-auto d-block" src={previewImage} alt="" />
          </div>
        )}

        {currentFile && (
          <div className="progress my-3">
            <div
              className="progress-bar progress-bar-info progress-bar-striped"
              role="progressbar"
              aria-valuenow={progress}
              aria-valuemin="0"
              aria-valuemax="100"
              style={{ width: progress + "%" }}
            >
              {progress}%
            </div>
          </div>
        )}

        <div className="row">
          <div className="col-8">
            <label className="btn btn-default p-0">
              <input type="file" accept="image/*" onChange={this.selectFile} />
            </label>
          </div>

          <div className="col-4">
            <button
              className="btn btn-success btn-sm"
              disabled={!currentFile}
              onClick={this.upload}
            >
              Upload
            </button>
          </div>
        </div>

        {detectedIngredients.length > 0 && (

          <div className="card mt-3">
            <div className="card-header">Detected ingredients</div>
            <ul className="list-group list-group-flush">
              {detectedIngredients.map((ingredient, index) => (
                      <li className="list-group-item" key={index}>
                        <span>{ingredient.ingredientCount.toString() + " " + (ingredient.ingredientCount < 2?ingredient.label:ingredient.label + "s")}</span>
                      </li>
                  ))}
            </ul>
            </div>
          )}


        {recipesCount > 0?<h4 className="text-center mt-4 mb-4">Найдены рецепты ({recipesCount})</h4>:<h4 className="text-center">Список рецептов пуст</h4>}
        
        {recipesCount > 0 &&(
            <div className="row row-cols-1 row-cols-md-2">
              {recipes.map((recipe, index) => (
                  <div className="card">
                  <div className="card-header">
                    <a href={recipe.url} target="_blank" className="text-center"><p className="text-center mb-0">{recipe.label}</p></a>
                  </div>
                  <ul className="list-group list-group-flush">
                    <li className="list-group-item" key="image">
                      <img className="rounded mx-auto d-block" src={recipe.image} alt={recipe.label}/>
                    </li>
                    <li className="list-group-item" key="calories">
                      <span style={{ fontWeight: "bold" }}>Calories:</span><span> {Math.round(recipe.calories)}</span>
                    </li>
                    <li className="list-group-item" key="weight">
                      <span style={{ fontWeight: "bold" }}>Total weight:</span><span> {Math.round(recipe.totalWeight)}</span>
                    </li>
                    <li className="list-group-item" key="mealType">
                      <span style={{ fontWeight: "bold" }}>Meal type:</span><span> {recipe.mealType.join(", ")}</span>
                    </li>
                    <li className="list-group-item" key="ingredients">
                      <span style={{ fontWeight: "bold" }}>Ingredients:</span><span> {recipe.ingredientLines.join("; ")}</span>
                    </li>
                  </ul>
                  </div>
              ))}
            </div>
        )}
        
      </div>
    );
  }
}
